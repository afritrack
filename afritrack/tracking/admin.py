from django.contrib import admin
from afritrack.tracking.models import Donor, PC, School, Hub, Country

admin.site.register(Donor)
admin.site.register(PC)
admin.site.register(School)
admin.site.register(Hub)
admin.site.register(Country)
