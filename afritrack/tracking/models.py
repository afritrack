from django.db import models

# Create your models here.

class Donor(models.Model):
    name = models.CharField(max_length=100)
    email = models.EmailField()

    def __str__(self):
        return self.name

class Country(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class School(models.Model):
    name = models.CharField(max_length=50)
    country = models.ForeignKey(Country)

    def __str__(self):
        return "%s (%s)" % (self.name, self.country.name)

class Hub(models.Model):
    name = models.CharField(max_length=50)
    country = models.ForeignKey(Country)

    def __str__(self):
        return "%s (%s)" % (self.name, self.country.name)
    

class PC(models.Model):
    donor = models.ForeignKey(Donor)
    when_donated = models.DateField()

    when_shipped_to_hub = models.DateField(blank=True, null=True)
    when_hub_recieved = models.DateField(blank=True, null=True)
    hub = models.ForeignKey(Hub, blank=True, null=True)

    when_delivered_to_school = models.DateField(blank=True, null=True)
    school = models.ForeignKey(School, blank=True, null=True)

    cpu_speed = models.CharField(max_length=10)

    def __str__(self):
        return "%s PC from %s" % (self.cpu_speed, self.donor.name)
